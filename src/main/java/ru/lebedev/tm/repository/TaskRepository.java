package ru.lebedev.tm.repository;

import ru.lebedev.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private static final Comparator<Task> NAME_COMPARATOR = new Comparator<Task>() {
        @Override
        public int compare(Task t1, Task t2) {
            if (t1.getName() != null && t2.getName() != null) {
                return t1.getName().compareTo(t2.getName());
            } else if (t1.getName() == null && t2.getName() != null) {
                return -1;
            } else if (t1.getName() != null && t2.getName() == null) {
                return 1;
            } else {
                return 0;
            }

        }

    };

    private List<Task> tasks = new ArrayList<>();
    private Map<String, List<Task>> tasksByName = new HashMap<>();


    public Task create(final String name) {
        final Task task = create(name, null);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        addTaskToMap(task);
        tasks.add(task);
        return task;
    }

    private void addTaskToMap(Task task) {
        String name = task.getName();
        List<Task> tasks = tasksByName.get(name);

        if (tasks == null){
          tasks = new ArrayList<>();
          tasks.add(task);
            tasksByName.put(name, tasks);
        } else {
            tasks.add(task);
        }


    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        final String currentName = task.getName();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        List<Task> nameIndex = findByNameIndex(currentName);
        if (!currentName.equals(name)) {
            if (nameIndex.size() > 1) {
                nameIndex.remove(task);
                tasksByName.put(currentName, nameIndex);
            } else tasksByName.remove(currentName);
            addTaskToMap(task);
        }
        return task;
    }

    public void clear() {
        tasksByName.clear();
        tasks.clear();
    }

    public int numberOfTasks() {
        return tasks.size();
    }

    public Task findByIndex(final int index) {
        List<Task> tasks = findAllOrderByName();
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        for (final Task task : tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;

    }

    public List<Task> findByNameIndex(final String name) {
        List<Task> nameTasks = tasksByName.get(name);
        return nameTasks;

    }

    public List<Task> findByNameIndex(final String name, final Long userId) {
        List<Task> nameTasks = tasksByName.get(name);
        List<Task> userTasks = new ArrayList<>();
        for(final Task task: nameTasks){
            if(task.getUserId().equals(userId)){
                userTasks.add(task);
            }
        }
        return userTasks;

    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (Objects.isNull(task)) return null;
        final String name = task.getName();
        List<Task> nameTask = tasksByName.get(name);
        if(nameTask.size() > 1) {
            nameTask.remove(task);
            tasksByName.put(name, nameTask);
        } else tasksByName.remove(name);
        tasks.remove(task);
        return task;

    }

    public List<Task> removeByName(final String name) {
        final List<Task> result = findByNameIndex(name);
        if (result.isEmpty()) return null;
        for(final Task task : result){
            tasks.remove(task);
        }
        tasksByName.remove(name);
        return result;

    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (Objects.isNull(task)) return null;
        final String name = task.getName();
        List<Task> nameTask = tasksByName.get(name);
        if(nameTask.size() > 1) {
            nameTask.remove(task);
            tasksByName.put(name, nameTask);
        } else tasksByName.remove(name);
        tasks.remove(task);
        return task;

    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (task.getProjectId().equals(projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task : tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;

    }

    public Task findByUserIdAndId(final Long userId, final Long taskId) {
        if (Objects.isNull(taskId)) return null;
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null) continue;
            if (!idUser.equals(userId)) continue;
            if (task.getId().equals(taskId)) return task;
        }
        return null;
    }

    public Task findById(final Long id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;

    }

   public List<Task> findAll() {
        return tasks;
    }
    /**
     * метод findAll деактивирован, т.к. создан новый - с выводом задач с сортировкой по имени
   */
 /* public List<Task> findAll(final Long userId) {
        if (!Objects.isNull(userTasks) || !userTasks.isEmpty()) userTasks.clear();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()))
                userTasks.add(task);
        }
        return userTasks;
    }
*/
    public List<Task> findAllOrderByName() {
        List<Task> copy = new ArrayList<>(tasks);
        Collections.sort(copy, NAME_COMPARATOR);
        return copy;
    }

    public List<Task> findAllOrderByName(final Long userId) {
       List<Task> copy = new ArrayList<>(tasks);
        //if (!Objects.isNull(userTasks) || !userTasks.isEmpty()) userTasks.clear();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId()))
                copy.add(task);
        }
        Collections.sort(copy, NAME_COMPARATOR);
        return copy;
    }


}
